# Writing yaml files

## Getting started

### 1. Create a fork of this project.

- Create a gitlab account if not already done. Go to [Gitlab sign-in page](https://gitlab.com/users/sign_in).
- Login into your gitlab account
- Go to this [repository homepage](https://gitlab.com/startx-demo/devops/yaml-basic) and click on the [fork button](https://gitlab.com/startx-demo/devops/yaml-basic/-/forks/new).
- Be sure that the fork you're creating is public.
- read the vim introduction if you need to now more on how to edit files [vim tutorial](https://www.tutorialspoint.com/vim/vim_getting_familiar.htm)

### 2. clone your project

When you are under you own copy of the repository, click on the clone button, and copy the http url to download your repository

```bash
git clone https://gitlab.com/<your_username>/yaml-basic.git (! don't forget the .git at the end)
cd yaml-basic
```

### 3. Configure your git environment

Because whe just newly installed git, git is not set with it's default values. We have to configure default value used when we will commit our changes.

```bash
git config --global user.name "My name"
git config --global user.email mymail@example.com
git config --global push.default simple
```

### 4. Create your first YAML file

Create a YAML file named **list.yml** with the following content
- one property named **name** and your name as a value
- one property named **playlist** with a list of to song you like. The list should be in json form

```yaml
name: christophe
playlist: ["Whish you where here", "Eyes of the tiger"]
```

Commit your change in the main branch with

```bash
git add list.yml
git commit -m "adding my first version of list.yml
```

### 5. New branches

#### 5.1. Create working branches

Create 2 git branch named **dev1** and **dev2**

```bash
git checkout -b dev1
git checkout -b dev2
```

#### 5.2. Work in the dev1 branch

Switch to the dev1 branch and add a new entry to your song list. Commit your changes.

```bash
git checkout dev1
```

```yaml
name: christophe
playlist: ["Whish you where here", "Eyes of the tiger", "Dark side of the moon"]
```

```bash
git add list.yml
git commit -m "adding Dark side of the moon"
```

#### 5.3. Work in the dev2 branch

Switch to the dev2 branch and add a new entry to your song list (differents from the one added to your dev1 song list). Commit your changes.

```bash
git checkout dev2
```

```yaml
name: christophe
playlist: ["Whish you where here", "Eyes of the tiger", "The wall part 2"]
```

```bash
git add list.yml
git commit -m "adding The wall part 2"
```

#### 5.4. Go back to the main branch

Switch to the main branch and observe that your list contain only the 2 first songs.

```bash
git checkout main
```

### 6. Merging

#### 6.1. Merge dev1 in main

Switch to the main branch (to be sure), and merge changes from dev1 branch. 
Your list contain now 3 songs (2 first song + the dev1 song).

```bash
git checkout main
git merge dev1
```

list.yml in main branch should be with 

```yaml
name: christophe
playlist: ["Whish you where here", "Eyes of the tiger", "Dark side of the moon"]
```

#### 6.2. Merge dev2 in main

Merge dev2 branch

```bash
git checkout main
git merge dev2
```

you have a conflic in list.yml that must be resolved. Resolve it by editing the file. You should keep 4 songs in your playlist at the end. 

```yaml
name: christophe
playlist: ["Whish you where here", "Eyes of the tiger", "Dark side of the moon", "The wall part 2"]
```

#### 6.3. Commit your merge.

```bash
git checkout main
git merge dev2
```

### 7. Merging

#### 7.1. Convert your YAML list

Change the format of your list to a YAML structure instead of a JSON one (use return and -, instead of [])

```yaml
name: christophe
playlist: 
  - "Whish you where here"
  - "Eyes of the tiger"
  - "Dark side of the moon"
  - "The wall part 2"
```

#### 7.2. Commit your change

```bash
git add list.yml
git commit -m "convert playlist into YAML list"
```

#### 7.3. Merge with dev1 and dev2 branches

Merge your change into the dev1 branch, resolve conflic if occurs
Change an existing song and add e new one at the end of the list
Commit your change and go back to the main branch
Merge your work from the dev1 branch (no conflict)

```bash
git checkout dev1
git merge main
# conflic resolution if occur then `git commit`
git checkout dev2
git merge main
# conflic resolution if occur then `git commit`
git checkout main
```

### 8. Push your work

Push your work to your remote repository

```bash
git push origin dev1 dev2 main
```
